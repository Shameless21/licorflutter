class UserModel {
  int? id;
  String? name;
  String? email;
  String? password;
  int? phone;

  UserModel({this.id, this.name, this.email, this.password, this.phone});

  factory UserModel.fromJson(Map<String, dynamic> parsedJson) {
    return UserModel(
        id: parsedJson['idUser'],
        name: parsedJson['name'],
        email: parsedJson['email'],
        password: parsedJson['password'],
        phone: parsedJson['phone']);
  }

  Map<String, dynamic> toJson() => {
        'idUser': id,
        'name': name,
        'email': email,
        'password': password,
        'phone': phone
      };

  Map<String, dynamic> toRegistry() =>
      {'name': name, 'email': email, 'password': password, 'phone': phone};
}
