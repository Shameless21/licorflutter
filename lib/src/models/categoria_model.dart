class CategoriaModel {
  int? id;
  String? name;
  String? description;

  CategoriaModel({
    this.id,
     this.name,
      this.description});

  factory CategoriaModel.fromJson(Map<String, dynamic> parsedJson) {
    return CategoriaModel(
        id: parsedJson['id'],
        name: parsedJson['name'],
        description: parsedJson ['description']);
  }

   Map <String,dynamic> toJson()=>{
     'id':id,
      'name':name,
     'description':description

   };

   Map <String,dynamic> toRegistry()=>{
      'name':name,
     'description':description
 };
}
