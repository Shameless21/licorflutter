import 'dart:convert';

  List<LicorModel> LicorModelFromJson(String str) => List<LicorModel>.from(json.decode(str).map((x) => LicorModel.fromJson(x)));

String albumsToJson(List<LicorModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class LicorModel {
  final String name;
  final String description;
  final int price;

  LicorModel({
    required this.name,
   required   this.description,
    required    this.price});

  factory LicorModel.fromJson(Map<String, dynamic> parsedJson) {
    return LicorModel(
        name: parsedJson['name'],
        description: parsedJson ['description'],
        price: parsedJson['price']);
  }

 factory LicorModel.fromJsonMap(Map parsedJson) {
    return LicorModel(
        name: parsedJson['name'],
        description: parsedJson ['description'],
        price: parsedJson['price']);
  }

   Map <String,dynamic> toJson()=>{
      'name':name,
     'description':description,
     'price':price

   };

   Map <String,dynamic> toRegistry()=>{
      'name':name,
     'description':description,
     'price':price
 };
}
