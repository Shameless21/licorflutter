class TiendaModel {
  int? id;
  String? name;
  String? ciudad;
  String? direccion;
  String? telefono;
  String? email;

  TiendaModel(
      {this.id,
      this.name,
      this.ciudad,
      this.direccion,
      this.telefono,
      this.email});

  factory TiendaModel.fromJson(Map<String, dynamic> parsedJson) {
    return TiendaModel(
        id: parsedJson['id'],
        name: parsedJson['name'],
        ciudad: parsedJson['ciudad'],
        direccion: parsedJson['direccion'],
        telefono: parsedJson['telefono'],
        email: parsedJson['email']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'ciudad': ciudad,
        'direccion': direccion,
        'telefono': telefono,
        'email': email
      };

  Map<String, dynamic> toRegistry() => {
        'name': name,
        'ciudad': ciudad,
        'direccion': direccion,
        'telefono': telefono,
        'email': email
      };
}
