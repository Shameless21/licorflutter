import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:licor/src/models/apiresponse_model.dart';
import 'package:licor/src/models/error_api_response_model.dart';

import 'package:licor/src/models/tienda_model.dart';

import '../models/apiresponse_model.dart';
import '../models/error_api_response_model.dart';
import '../models/tienda_model.dart';

class TiendaService {
  Future<ApiResponse> insertTienda(TiendaModel tiendaModel) async {
    var body = json.encode(tiendaModel.toRegistry());
    var apiResponse = ApiResponse(menssage: '', object: '', statusResponse: 0);
    String baseUrl = 'http://192.168.1.8:8085';

    var res = await http.post('$baseUrl/api/licorApp/Tienda/save',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 0) {
      var _tiendaModel = TiendaModel.fromJson(resBody);
    } else {
      var _error = ErrorApiResponse.fromJson(resBody);
    }
    return apiResponse;
  }
}
