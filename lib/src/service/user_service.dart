import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import '../models/apiresponse_model.dart';
import '../models/error_api_response_model.dart';
import '../models/user_model.dart';

class UserService {
  Future<ApiResponse> insertUser(UserModel userModel) async {
    var body = json.encode(userModel.toRegistry());
    var apiResponse = ApiResponse(menssage: '', object: '', statusResponse: 0);
    String baseUrl = 'http://192.168.1.8:8085';

    var res = await http.post('$baseUrl/api/licorApp/User/save',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 0) {
      var userModel = UserModel.fromJson(resBody);
    } else {
      var _error = ErrorApiResponse.fromJson(resBody);
    }
    return apiResponse;
  }
}
