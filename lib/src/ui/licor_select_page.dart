
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:licor/src/bloc/licor_bloc.dart';
import 'package:licor/src/models/licor_model.dart';

class LicorSelect extends StatefulWidget{
  const LicorSelect({Key? key}) : super(key: key);

   @override
  LicorSelectScreen createState() {
    return LicorSelectScreen();
  }

}

class LicorSelectScreen extends State<LicorSelect>{
 LicorBloc licorBloc = new LicorBloc();
   late Future<List<LicorModel>> data;

  @override
  void initState() {
    super.initState();
    data = licorBloc.getLicor();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Licores'),
        actions: [
          Chip(
            label: StreamBuilder<int>(
                builder: (context, snapshot) {
                  return Text(
                    (snapshot.data ?? 0).toString(),
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  );
                }),
            backgroundColor: Colors.red,
          ),
          Padding(
            padding: EdgeInsets.only(right: 16),
          )
        ],
      ),
         body: Center(
        child: FutureBuilder<List<LicorModel>>(
            future: data,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(snapshot.data![index].name),
                        subtitle: Text(snapshot.data![index].description),
                      );
                    });
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              } else {
                return CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}