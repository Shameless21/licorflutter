import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:licor/src/bloc/categoria_bloc.dart';
import 'package:licor/src/models/categoria_model.dart';
import 'package:licor/src/repository/repository.dart';

class CategoriaPage extends StatefulWidget {
  const CategoriaPage({Key? key}) : super(key: key);
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  @override
  CategoriaFormState createState() {
    return CategoriaFormState();
  }
}

class CategoriaFormState extends State<CategoriaPage> {
  TextEditingController nombreController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  CategoriaBloc categoriaBloc = new CategoriaBloc();
  Repository repository = new Repository();
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: nombreController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su nombre',
              labelText: 'Nombre',
            ),
          ),
          TextFormField(
            controller: descriptionController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su descripcion',
              labelText: 'Descripcion',
            ),
          ),
          new Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new TextButton(
                  child: const Text('Guardar'),
                  onPressed: () async {
                    try {
                      CategoriaModel categoriaModel = new CategoriaModel(
                          id: null,
                          name: nombreController.text,
                          description: descriptionController.text);
                      categoriaBloc.insertCategoria(categoriaModel);
                    } on FormatException catch (e) {
                      print(e);
                    }
                  })),
        ],
      ),
    );
  }
}
