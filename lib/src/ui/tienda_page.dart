import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:licor/src/bloc/tienda_bloc.dart';
import 'package:licor/src/models/tienda_model.dart';
import 'package:licor/src/repository/repository.dart';

class TiendaPage extends StatefulWidget {
  const TiendaPage({Key? key}) : super(key: key);
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  @override
  TiendaFormState createState() {
    return TiendaFormState();
  }
}

class TiendaFormState extends State<TiendaPage> {
  TextEditingController nombreController = new TextEditingController();
  TextEditingController ciudadController = new TextEditingController();
  TextEditingController direccionController = new TextEditingController();
  TextEditingController telefonoController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TiendaBloc tiendaBloc = new TiendaBloc();
  Repository tienda_repository = new Repository();
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: nombreController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su nombre',
              labelText: 'Nombre',
            ),
          ),
          TextFormField(
            controller: ciudadController,
            decoration: const InputDecoration(
              hintText: 'Ingrese la ciudad',
              labelText: 'ciudad',
            ),
          ),
          TextFormField(
            controller: direccionController,
            decoration: const InputDecoration(
              hintText: 'Ingrese direccion',
              labelText: 'direccion',
            ),
          ),
          TextFormField(
            controller: telefonoController,
            decoration: const InputDecoration(
              hintText: 'Ingrese el telefono',
              labelText: 'telefono',
            ),
          ),
          TextFormField(
            controller: emailController,
            decoration: const InputDecoration(
              hintText: 'Ingrese email',
              labelText: 'email',
            ),
          ),
          new Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new TextButton(
                  child: const Text('Submit'),
                  onPressed: () async {
                    try {
                      TiendaModel tiendaModel = new TiendaModel(
                          id: null,
                          name: nombreController.text,
                          ciudad: ciudadController.text,
                          direccion: direccionController.text,
                          telefono: telefonoController.text,
                          email: emailController.text);
                      tiendaBloc.insertTienda(tiendaModel);
                    } on FormatException catch (e) {
                      print(e);
                    }
                  })),
        ],
      ),
    );
  }
}
