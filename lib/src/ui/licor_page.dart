import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:licor/src/bloc/licor_bloc.dart';
import 'package:licor/src/models/licor_model.dart';
import 'package:licor/src/repository/repository.dart';

class LicorPage extends StatefulWidget {
  const LicorPage({Key? key}) : super(key: key);
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  @override
  LicorFormState createState() {
    return LicorFormState();
  }
}

class LicorFormState extends State<LicorPage> {
  TextEditingController nombreController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  LicorBloc licorBloc = new LicorBloc();
  Repository repository = new Repository();
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: nombreController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su nombre',
              labelText: 'Nombre',
            ),
          ),
          TextFormField(
            controller: descriptionController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su descripcion',
              labelText: 'Descripcion',
            ),
          ),
          TextFormField(
            controller: priceController,
            decoration: const InputDecoration(
              hintText: 'Ingrese su precio',
              labelText: 'Precio',
            ),
          ),
          new Container(
              padding: const EdgeInsets.only(left: 150.0, top: 40.0),
              child: new TextButton(
                  child: const Text('Guardar'),
                  onPressed: () async {
                    try {
                      LicorModel licorModel = new LicorModel(
                          name: nombreController.text,
                          description: descriptionController.text,
                          price: 50);
                      licorBloc.insertLicor(licorModel);
                    } on FormatException catch (e) {
                      print(e);
                    }
                  })),
        ],
      ),
    );
  }
}
