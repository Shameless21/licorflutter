import 'package:flutter/material.dart';
import 'package:licor/src/ui/licor_page.dart';
import 'package:licor/src/ui/licor_select_page.dart';
import 'package:licor/src/ui/tienda_page.dart';

import '../ui/categoria_page.dart';
import '../ui/user_page.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Licor APP',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 95, 196, 255),
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/images/cover.jpg'))),
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Crear licor'),
                onTap: () => {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => LicorPage()))
            },
          ),
          ListTile(
            leading: Icon(Icons.input),
            title: Text('Consultar licor'),
                onTap: () => {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => LicorSelect()))
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Categoria'),
            onTap: () => {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => CategoriaPage()))
            },
          ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('usuarios'),
            onTap: () => {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => UserPage()))
            },
          ),
           ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Tienda'),
            onTap: () => {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => TiendaPage()))
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
