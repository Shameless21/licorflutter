import 'package:licor/src/models/apiresponse_model.dart';
import 'package:licor/src/models/tienda_model.dart';
import 'package:licor/src/repository/repository.dart';
import 'package:licor/src/service/tienda_service.dart';

import '../models/apiresponse_model.dart';
import '../models/tienda_model.dart';
import '../repository/repository.dart';

class TiendaBloc {
  Repository repository = new Repository();

  Future<ApiResponse> insertTienda(TiendaModel tiendaModel) async {
    var apiResponse = await repository.insertTienda(tiendaModel);
    return apiResponse;
  }
}
