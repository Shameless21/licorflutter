import 'package:licor/src/models/apiresponse_model.dart';
import 'package:licor/src/models/categoria_model.dart';
import 'package:licor/src/repository/repository.dart';

import 'package:licor/src/service/categoria_service.dart';

import '../models/apiresponse_model.dart';
import '../models/categoria_model.dart';
import '../repository/repository.dart';

class CategoriaBloc {
  Repository repository = new Repository();

  Future<ApiResponse> insertCategoria(CategoriaModel categoriaModel) async {
    var apiResponse = await repository.insertCategoria(categoriaModel);
    return apiResponse;
  }
}
