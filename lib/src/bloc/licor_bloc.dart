import 'dart:async';

import 'package:licor/src/models/apiresponse_model.dart';
import 'package:licor/src/models/licor_model.dart';
import 'package:licor/src/repository/repository.dart';
import 'package:licor/src/service/licor_service.dart';
import '../models/apiresponse_model.dart';
import '../models/licor_model.dart';
import '../repository/repository.dart';

class LicorBloc {
  Repository repository = new Repository();
  LicorService licorService= new LicorService();
   final StreamController<int> _licorCouenter = StreamController<int>();

  Future<ApiResponse> insertLicor(LicorModel licorModel) async {
    var apiResponse = await repository.insertLicor(licorModel);
    return apiResponse;
  }

 Future<List<LicorModel>>  getLicor  () {
     return repository.getLicor();
  }


}
