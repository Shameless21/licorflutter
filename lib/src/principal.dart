import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:licor/src/widgets/NavDrawer.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'applicor',
      routes: {},
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('menu'),
      ),
      body: Center(
        child: Image(
            image: NetworkImage(
                'https://images.vexels.com/media/users/3/202539/isolated/lists/7aec004f4520a7ba5d4399a774e5b6e4-botella-de-vino-negro.png'),
            height: 640,
            width: 600
            //child: Text('Side Menu Tutorial'),
            ),
      ),
    );
  }
}
