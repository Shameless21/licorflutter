import 'package:licor/src/models/apiresponse_model.dart';
import 'package:licor/src/models/categoria_model.dart';
import 'package:licor/src/models/tienda_model.dart';
import 'package:licor/src/service/licor_service.dart';
import 'package:licor/src/service/tienda_service.dart';

import '../models/apiresponse_model.dart';
import '../models/categoria_model.dart';
import '../models/licor_model.dart';
import '../models/user_model.dart';
import '../service/categoria_service.dart';
import '../service/licor_service.dart';
import '../service/user_service.dart';

class Repository {
  final licorService = new LicorService();
  final tiendaService = new TiendaService();
  final userService = new UserService();
  final categoriaService = new CategoriaService();

  Future<ApiResponse> insertLicor(LicorModel licorModel) =>
      licorService.insertLicor(licorModel);

  Future<List<LicorModel>> getLicor() {
    return licorService.getLicor();
  }

  Future<ApiResponse> insertUser(UserModel userModel) =>
      userService.insertUser(userModel);

  Future<ApiResponse> insertCategoria(CategoriaModel categoriaModel) =>
      categoriaService.insertCategoria(categoriaModel);

  Future<ApiResponse> insertTienda(TiendaModel tiendaModel) =>
      tiendaService.insertTienda(tiendaModel);
}
